;;; -*- lexical-binding: t; -*-
;; emacs-version:   GNU Emacs 29.0.60
;; emacs-init-time: (gui) .67, (tty) .29

(defun open-init () (interactive) (find-file user-init-file))
(defun random-choice (items) (nth (random (length items)) items))
(defun show-time () (interactive) (message (format-time-string "%H:%M")))

;; ------------------------------------------------------------------------

;; editfns.c
(setq user-full-name "Andrij Mizyk")

;; lread.c
(push "~/.emacs.d/packs" load-path)
(load "~/.emacs.d/quotes.el")

;; frame.c
(setq default-frame-alist
 '((width . 80)(height . 24)(tool-bar-lines . 0)(vertical-scroll-bars . left)
   (font . "DejaVu Sans Mono - Bront 10")))

;; xdisp.c
(setq frame-title-format "%b")

;; casefiddle.c
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; fns.c
(setq use-short-answers t)
;(setq use-dialog-box nil)

;; dispnew.c
(setq visible-bell t)

;; callproc.c
(setq shell-file-name "/usr/bin/sh")

;; ........................................................................

;; startup.el
(setq
 user-mail-address "andm1zyk@proton.me"
 inhibit-startup-screen t
 ;initial-major-mode 'text-mode
 initial-scratch-message (concat ";; " (random-choice quotes)))

;; simple.el
(column-number-mode t)

;; dired.el
(add-hook 'dired-after-readin-hook 'hl-line-mode)

;; ido.el
(ido-mode 'buffers)

;; elec-pair.el
(setq electric-pair-pairs '((8222 . 8220)))
(electric-pair-mode 1)

;; files.el
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))

;; recentf.el
(recentf-mode 1)

;; time.el
(setq
 display-time-default-load-average nil
 display-time-24hr-format t
 display-time-mail-string "")

(if (not (display-graphic-p)) (display-time))

;; electric.el
(electric-indent-mode 0)

;; remember.el
(setq remember-time-format "%a, %d.%m.%Y %H:%M:%S")

;; man.el
(setq Man-notify-method 'pushy)

;; custom.el
(setq custom-theme-directory "~/.emacs.d/themes")
;(load-theme 'gruvbox-dark-hard t)
;(disable-theme 'gruvbox-dark-hard)

;; faces.el
;(set-face-attribute 'font-lock-comment-face nil :slant 'italic)
; bront не робиться курсивним 					;

;; erc.el
(setq
 erc-nick "axmed99"
 erc-server "irc.eu.libera.chat"
 erc-port 6667
 erc-prompt-for-password nil)

;; net/browse-url.el
(setq
 browse-url-browser-function 'browse-url-generic
 browse-url-generic-program "librewolf")

;; vc/add-log.el
(setq change-log-default-name "CHANGELOG")

;; gnus/message.el
(setq message-auto-save-directory "~/.emacs.d/drafts")

;; org/org.el
(setq
 org-directory "~/org"
 org-todo-keywords
 '((sequence "TODO" "|" "DONE")
   (sequence "ЗРОБ" "|" "ГОТОВО")))
(add-hook 'org-mode-hook (lambda () (setq fill-column 79) (turn-on-auto-fill)))

;; org/ol.el
(setq org-link-abbrev-alist
 '(("github" . "https://github.com")
   ("gitlab" . "https://github.com")))

;; ------------------------------------------------------------------------

;; tempo.el
(require 'tempo)
(tempo-define-template "raku-lang" '("#!/usr/bin/env raku") "envraku")

;; ........................................................................

;; progmodes/prog-mode.el
(add-hook 'prog-mode-hook (lambda () (hs-minor-mode t)))

;; progmodes/cc-vars.el
(setq c-default-style "linux")

;; progmodes/ruby-mode.el
(add-hook 'ruby-mode-hook (lambda () (electric-indent-mode 1)))

;; progmodes/sh-script.el
(setq
 sh-basic-offset 8
 sh-shell-file "/usr/bin/sh")

;; progmodes/sql.el
(setq sql-product 'postgres)

;; ........................................................................

;; textmodes/css-mode.el
(setq css-indent-offset 2)

;; textmodes/sgml-mode.el
(setq sgml-basic-offset 8)
(push '("\\.wml\\'" . html-mode) auto-mode-alist)

;; ------------------------------------------------------------------------

;; csv-mode.el
(autoload 'csv-mode "csv-mode" "Осн. режим для редагування файлів csv." t)
(push '("\\.[Cc][Ss][Vv]\\'" . csv-mode) auto-mode-alist)

;; markdown-mode.el
(autoload 'gfm-mode "markdown-mode" "Осн. режим для різновидів Markdown." t)
(autoload 'markdown-mode "markdown-mode" "Осн. режим для файлів Markdown." t)
(push '("README\\.md\\'" . gfm-mode) auto-mode-alist)
(push '("\\.md\\'" . markdown-mode) auto-mode-alist)

;; org-journal.el
(require 'org-journal)
(setq org-journal-dir "~/org/journal/")

;; po-mode.el
(autoload 'po-mode "po-mode" "Осн. режим для редагування файлів PO" t)
(push '("\\.po\\'\\|\\.po\\." . po-mode) auto-mode-alist)

;; raku-mode.el
(require 'raku-mode)
(autoload 'raku-mode "raku-mode" "Осн. режим для редагування скриптів Raku." t)
(setq raku-exec-path "~/.rakudo/bin/rakudo")
(setq raku-indent-offset 8)

;; ukrainian-calendar.el
(require 'ukrainian-calendar)

;; ------------------------------------------------------------------------

(keymap-global-set "<f12>" 'open-init)
(keymap-global-set "<f7>" 'calendar)
(keymap-global-set "<f6>" 'shell)
(keymap-global-set "<f5>" 'man)

(keymap-global-set "C-c r" 'recentf-open-files)
(keymap-global-set "C-c j" 'org-journal-new-entry)
(keymap-global-set "C-c w" 'whitespace-mode)
(keymap-global-set "C-c SPC" 'tempo-complete-tag)
(keymap-global-set "C-x M-r" 'remember)
(keymap-global-set "C-c a" 'org-agenda)
(keymap-global-set "C-c n" 'display-line-numbers-mode)
(keymap-global-set "C-c t" 'todo-show)
(keymap-global-set "C-c C-t" 'show-time)

(keymap-set key-translation-map "C-x 8 i" "∞")

;;; .emacs ends here
