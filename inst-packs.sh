#!/usr/bin/sh
# -------------------------------------------------------------------------
packs_d=~/.emacs.d/packs
github=https://github.com
gnuelpa=http://elpa.gnu.org/packages
nongnuelpa=https://elpa.nongnu.org/nongnu
# -------------------------------------------------------------------------

mkdir -pv $packs_d/
mkdir -pv ~/org/
mkdir -v ~/.emacs.d/themes/

# -------------------------------------------------------------------------

# bront (шрифт)
git clone $github/chrismwendt/bront.git
su -c 'mkdir /usr/share/fonts/truetype/bront/ && cp -v bront/*.ttf /usr/share/fonts/bront/'
rm -rf bront/

# csv-mode
wget $gnuelpa/csv-mode-1.21.tar
tar xvf csv-mode-*.tar
cp -v csv-mode-*/csv-mode.el $packs_d/
rm -rf csv-mode-*.tar csv-mode-*/

# markdown-mode
wget $nongnuelpa/markdown-mode-2.5.tar
tar xvf markdown-mode-*.tar
cp -v markdown-mode-*/markdown-mode.el $packs_d/
rm -rf markdown-mode-*.tar markdown-mode-*/

# org-journal
git clone $github/bastibe/org-journal.git
cp -v org-journal/*.el $packs_d/
rm -rf org-journal/

# po-mode
git clone $github/emacsmirror/po-mode.git
cp -v po-mode/po-mode.el $packs_d/
rm -rf po-mode/

# raku-mode
git clone $github/Raku/raku-mode.git
cp -v raku-mode/*.el $packs_d/
rm -rf raku-mode/

# ukrainian-calendar
cp -v ukrainian-calendar.el/*.el $packs_d/

# xwiki-mode
git clone $github/ackerleytng/xwiki-mode.git
cp -v xwiki-mode/xwiki-mode.el $packs_d/
rm -rf xwiki-mode/

# simple-wiki-mode
curl -o $packs_d/simple-wiki.el \
    "http://cvs.savannah.gnu.org/viewvc/*checkout*/http-emacs/http-emacs/simple-wiki.el"

# emacs-theme-gruvbox
git clone $github/jasonm23/autothemer
git clone $github/Greduan/emacs-theme-gruvbox
cp -v autothemer/autothemer.el $packs_d/
cp -v emacs-theme-gruvbox/gruvbox-dark-hard-theme.el ~/.emacs.d/themes/
cp -v emacs-theme-gruvbox/gruvbox.el $packs_d/
rm -rfv autothemer/ emacs-theme-gruvbox/

# ess
git clone $github/emacs-ess/ESS.git
cd ESS/ && make && cd ..
mv -v ESS $packs_d/

# graphviz-dot-mode
git clone $github/ppareit/graphviz-dot-mode.git
cp -v graphviz-dot-mode/graphviz-dot-mode.el $packs_d/
rm -rf graphviz-dot-mode/

# -------------------------------------------------------------------------
# не потрібні, якщо є tree-sitter

# dockerfile-mode
git clone $github/spotify/dockerfile-mode.git
sed -i "s/(require 's)/;(require 's)/g" dockerfile-mode/dockerfile-mode.el
cp -v dockerfile-mode/dockerfile-mode.el $packs_d/
rm -rf dockerfile-mode/

# rust-mode
git clone $github/rust-lang/rust-mode.git
cp -v rust-mode/*.el $packs_d/
rm -rf rust-mode/

# toml-mode
git clone $github/dryman/toml-mode.el.git
cp -v toml-mode.el/toml-mode.el $packs_d/
rm -rf toml-mode.el/

# yaml-mode
git clone $github/yoshiki/yaml-mode.git
cp -v yaml-mode/yaml-mode.el $packs_d/
rm -rf yaml-mode/

# -------------------------------------------------------------------------

cp -v init.el ~/.emacs

printf "готово.\n"

### inst-packs.sh ends here
